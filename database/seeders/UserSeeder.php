<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
        	[ 'name' => 'Alex', 'email' => 'alex@correo.com', 'password' => bcrypt('12345678'), 'role' => 'user','created_at' => date('Y-m-d H:i:s')],
        	[ 'name' => 'Jason', 'email' => 'jason@correo.com', 'password' => bcrypt('12345678'), 'role' => 'manager', 'created_at' => date('Y-m-d H:i:s')],
        	[ 'name' => 'Alice', 'email' => 'alice@correo.com', 'password' => bcrypt('12345678'), 'role' => 'admin', 'created_at' => date('Y-m-d H:i:s')],
        ]);
    }
}

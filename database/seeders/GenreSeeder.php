<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GenreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('genres')->insert([
            [ 'name' => 'Rock','created_at' => date('Y-m-d H:i:s')],
            [ 'name' => 'Jazz', 'created_at' => date('Y-m-d H:i:s')],
            [ 'name' => 'Grunge', 'created_at' => date('Y-m-d H:i:s')],
            [ 'name' => 'Metal', 'created_at' => date('Y-m-d H:i:s')],
        ]);

    }
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TrackSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tracks')->insert([
        	[ 'name' => 'Even Flow', 'genreId' => 3, 'composer' => 'Pearl Jam', 'unitPrice' => 12.5, 'created_at' => date('Y-m-d H:i:s')],
        	[ 'name' => 'One', 'genreId' => 4, 'composer' => 'Metallica', 'unitPrice' => 15, 'created_at' => date('Y-m-d H:i:s')],
        	[ 'name' => 'Black Hole Sun', 'genreId' => 1, 'composer' => 'SoundGarden', 'unitPrice' => 20, 'created_at' => date('Y-m-d H:i:s')],
        	[ 'name' => 'Cherub Rock', 'genreId' => 1, 'composer' => 'Smashing Pumpkins', 'unitPrice' => 25, 'created_at' => date('Y-m-d H:i:s'),],
        	[ 'name' => 'You give me something', 'genreId' => 2, 'composer' => 'Jamiroquai', 'unitPrice' => 30, 'created_at' => date('Y-m-d H:i:s'),],
        ]);
    }
}

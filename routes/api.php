<?php

if(version_compare(PHP_VERSION, '7.2.0', '>=')) {
    error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
}

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::get('/genres', 'Api\v1\GenreController@index');
Route::get('/genres/{id}','Api\v1\GenreController@show');

Route::get('/tracks','Api\v1\TrackController@index');
Route::get('/tracks/{id}','Api\v1\TrackController@show');


Route::post('/login', 'Api\v1\PassportAuthController@login');
  
Route::middleware('auth:api')->group(function () {
	
    Route::get('/get-user', 'Api\v1\PassportAuthController@userInfo');
    Route::post('/register', 'Api\v1\PassportAuthController@register')->middleware('can:isAdmin');
 
	Route::post('/genres', 'Api\v1\GenreController@store')->middleware('can:isUser');
	Route::put('/genres/{id}','Api\v1\GenreController@update')->middleware('can:isManager');
	Route::delete('/genres/{id}','Api\v1\GenreController@destroy')->middleware('can:isAdmin');
 
	Route::post('/tracks', 'Api\v1\TrackController@store')->middleware('can:isUser');
	Route::put('/tracks/{id}','Api\v1\TrackController@update')->middleware('can:isManager');
	Route::delete('/tracks/{id}','Api\v1\TrackController@destroy')->middleware('can:isAdmin');


});


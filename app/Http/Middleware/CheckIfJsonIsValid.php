<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response as HttpResponse;

class CheckIfJsonIsValid
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {

        $method = $request->method();

        if($request->isMethod('post') || $request->isMethod('put') ){
            json_decode($request->getContent());

            if (json_last_error() != JSON_ERROR_NONE) {
                // There was an error
                return response()->json([ 'message' => 'The request is not a valid JSON' ] ,HttpResponse::HTTP_BAD_REQUEST);
            }    
        }

        

        // JSON decoding didn’t throw error; continue
        return $next($request);
    }
}

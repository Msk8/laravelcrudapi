<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TrackResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'composer' => $this->composer,
            'unitPrice' => $this->unitPrice,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'genre' => new GenreResource($this->whenLoaded('genres')),
        ];
    }
}

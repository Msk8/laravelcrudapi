<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Models\Track;
use Illuminate\Http\Request;
use App\Http\Resources\TrackCollection;
use App\Http\Resources\TrackResource;
use Symfony\Component\HttpFoundation\Response as HttpResponse;

class TrackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new TrackCollection(Track::with(['genres'])->paginate());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([ 'name' => 'required|min:4|max:255', 'genreId' => 'required|numeric|min:1|exists:genres,id' , 'composer' => 'required|min:4|max:255', 'unitPrice' => 'required|numeric|min:1', ]);
        $track = Track::create([ 'name' => $request->name, 'genreId' => $request->genreId, 'composer' => $request->composer, 'unitPrice' => $request->unitPrice ]);
        return (new TrackResource(Track::with(['genres'])->findOrFail($track->id)))->response()->setStatusCode(HttpResponse::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Track  $track
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        return new TrackResource(Track::with(['genres'])->findOrFail($request->id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Track  $track
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
         $request->validate([ 'name' => 'required|min:4|max:255', 'genreId' => 'required|numeric|min:1' , 'composer' => 'required|min:4|max:255', 'unitPrice' => 'required|numeric|min:1', ]);
        Track::findOrFail($request->id)->update([ 'name' => $request->name, 'genreId' => $request->genreId, 'composer' => $request->composer, 'unitPrice' => $request->unitPrice ]);
        return new TrackResource(Track::with(['genres'])->findOrFail($request->id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Track  $track
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        Track::findorFail($request->id)->delete();
        return (new TrackCollection(Track::with(['genres'])->paginate()))->additional(['message' => 'Track Deleted!' ]);
    }
}

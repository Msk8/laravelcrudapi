<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Symfony\Component\HttpFoundation\Response as HttpResponse;

class PassportAuthController extends Controller
{
     /**
     * Registration Req
     */
    public function register(Request $request)
    {
    	
        $this->validate($request, [
            'name' => 'required|min:4',
            'email' => 'required|email',
            'password' => 'required|min:8',
            'role' => 'required|min:4'
        ]);
  		
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'role' => $request->role
        ]);
  		
        $token = $user->createToken('Laravel8PassportAuth')->accessToken;
  		
        return response()->json(['token' => $token], HttpResponse::HTTP_OK);
    }
  
    /**
     * Login Req
     */
    public function login(Request $request)
    {
        $data = [
            'email' => $request->email,
            'password' => $request->password
        ];
  
        if (auth()->attempt($data)) {
            $token = auth()->user()->createToken('Laravel8PassportAuth')->accessToken;
            return response()->json(['token' => $token], HttpResponse::HTTP_OK);
        } else {
            return response()->json(['error' => 'Unauthorised'], HttpResponse::HTTP_UNAUTHORIZED);
        }
    }
 
    public function userInfo() 
    {
 
    	$user = auth()->user();
      
    	return response()->json(['user' => $user], HttpResponse::HTTP_OK);
 
    }
}

<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Models\Genre;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\GenreCollection;
use App\Http\Resources\GenreResource;
use Symfony\Component\HttpFoundation\Response as HttpResponse;

class GenreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    
    public function index()
    {
        return new GenreCollection(Genre::with(['tracks'])->paginate());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([ 'name' => 'required|min:4|max:255', ]);
        $genre = Genre::create([ 'name' => $request->name ]);
        return (new GenreResource(Genre::findOrFail($genre->id)))->response()->setStatusCode(HttpResponse::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Genre  $genre
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        return new GenreResource(Genre::with(['tracks'])->findOrFail($request->id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Genre  $genre
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request->validate([ 'name' => 'required|min:4|max:255', ]);
        Genre::findOrFail($request->id)->update(['name' => $request->input('name')]);
        return new GenreResource(Genre::with(['tracks'])->findOrFail($request->id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Genre  $genre
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        Genre::findorFail($request->id)->delete();
        return (new GenreCollection(Genre::with(['tracks'])->paginate()))->additional(['message' => 'Genre Deleted!' ]);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Genre;

class Track extends Model
{
    use HasFactory;

    protected $table = 'tracks';

    protected $fillable = ['name','genreId','composer','unitPrice'];

    public function genres(){
    	return $this->belongsTo(Genre::class, 'genreId');
    }
}

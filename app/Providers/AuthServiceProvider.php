<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Illuminate\Auth\Access\Response;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
         'App\Models\Model' => 'App\Policies\ModelPolicy',
        //'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        
        Passport::routes();


        Gate::define('isAdmin', function($user) {
           return $user->role == 'admin' ? Response::allow() : Response::deny("You are unauthorized! Only Admins can use this endpoint! ");
        });

        Gate::define('isManager', function($user) {
            return $user->role == 'manager' ? Response::allow() : Response::deny("You are unauthorized! Only Managers can use this endpoint! ");
        });

        Gate::define('isUser', function($user) {
            return $user->role == 'user' ? Response::allow() : Response::deny("You are unauthorized! Only Users can use this endpoint!");
        });

    }
}
